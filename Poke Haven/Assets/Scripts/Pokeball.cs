using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pokeball : MonoBehaviour
{
    public GameObject parent;
    // public GameObject pokemon;
    public PokemonData pokemonData;
    private Inventory inventoryScript;
    public VrPhysicsButton validateButtonPush;
    public bool isTouching;
    
    // Start is called before the first frame update
    void Start()
    {
        parent = GetComponentInParent<Pokeball>().parent;
        inventoryScript = GetComponent<Inventory>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator InstantiatePokemon()
    {
        yield return new WaitForSeconds(2);
        Instantiate(pokemonData.prefabPokemon, parent.transform.position,transform.rotation);
        yield return new WaitForSeconds(0.01f);
        Destroy(parent);
    }

    void AddItem()
    {
        inventoryScript.pokemonData.Add(pokemonData);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            StartCoroutine(InstantiatePokemon());
        }
        if (other.CompareTag("PokeballDetection"))
        {
            Debug.Log("Jsuis dedans sah");
            validateButtonPush.obPressed.RemoveAllListeners();
            //validateButtonPush.SetActive(true);
            var ui = FindObjectOfType<UIManager>();
            validateButtonPush.obPressed.AddListener(() => ui.AddPokemonToInventory(this));
            //Destroy(gameObject);
            //validateButtonPush.obPressed.AddListener(ui.AddPokemonToInventory);
            isTouching = true;
        }
    }

    void OnTriggerStay(Collider other)
    {
        
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PokeballDetection"))
        {
            Debug.Log("Jsuis plus dedans");
            //validateButtonPush.SetActive(true);
            //validateButtonPush.obPressed.RemoveAllListeners();
            isTouching = false;
        }
    }
}
