using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseLook : MonoBehaviour
{

    public float mouseSensivity = 100f;

    public Transform playerBody;

    private float xRotation = 0f;
    
    
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetKey(KeyCode.Mouse0))
        // {
        //     Debug.Log("je clique");
        //     TEST();
        // }
        
        
        float mouseX = Input.GetAxis("Mouse X") * mouseSensivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensivity * Time.deltaTime;
        

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
        
        
    }
    
    bool RaycastUI(Vector3 origin, Vector3 direction, out GameObject obj) {
        obj = null;
        if(Physics.Raycast(origin, direction.normalized, out RaycastHit hit, direction.magnitude, 1 << LayerMask.NameToLayer("UI"))) {
            obj = hit.collider.gameObject;
            Debug.Log(hit.collider.name);
            return true;
        }
        return false;
    }
    
     void TEST() {
         
         Camera cam = Camera.main;
         Debug.DrawRay(cam.transform.position, cam.transform.forward * 100, Color.green);
         if(RaycastUI(cam.transform.position, cam.transform.forward * 100, out GameObject obj)) {
             // if a UI obj has been touched
                 Button btn = obj.GetComponent<Button>();
                 if(btn != null) {
                     // if the element is a button
                     btn.onClick?.Invoke(); // activate the button
                 }
         }
    }
}
