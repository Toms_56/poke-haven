using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PokemonData", menuName = "Poke Haven/PokemonData")]
public class PokemonData : ScriptableObject
{
    public enum Type
    {
        Fire, 
        Water,
    }

    public Sprite logo;
    public Type type;
    public string name = "";
    public float speed = 0;
    public GameObject prefabPokemon;
    public GameObject pokeball;
}
