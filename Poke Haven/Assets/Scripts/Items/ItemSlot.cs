using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour
{
    private PokemonData _pokemonData;
    [SerializeField] Image image;
    [SerializeField] GameObject prefab;
    public GameObject pokeballSpawner;


    private void Start()
    {
        pokeballSpawner = GetComponent<ItemSlot>().pokeballSpawner;
    }

    public PokemonData pokemonData
    {
        get { return _pokemonData; }
        set
        {
            _pokemonData = value;
            if (_pokemonData == null)
            {
                image.enabled = false;
            }
            else
            {
                image.sprite = _pokemonData.logo;
                image.enabled = true;
                prefab = _pokemonData.pokeball;
            }
        }
    }
    
    private void OnValidate()
    {
        if (image == null)
        {
            image = GetComponent<Image>();
            prefab = GetComponent<PokemonData>().pokeball;
        }
    }

    public void SpawnPokemon()
    {
        Instantiate(prefab, pokeballSpawner.transform.position, pokeballSpawner.transform.rotation);
    }
}
