using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<PokemonData> pokemonData;
    [SerializeField] private Transform itemsParents;
    public ItemSlot[] itemSlots;

    private void OnValidate()
    {
        if (itemsParents != null)
        
            itemSlots = itemsParents.GetComponentsInChildren<ItemSlot>();
        RefreshUi();
    }

    void RefreshUi()
    {
        int i = 0;
        for (; i<pokemonData.Count && i< itemSlots.Length;i++)
        {
            itemSlots[i].pokemonData = pokemonData[i];
        }

        for (;i<itemSlots.Length;i++)
        {
            itemSlots[i].pokemonData = null;
        }
    }

    public void AddPokemon(PokemonData data)
    {
        pokemonData.Add(data);
        RefreshUi();
    }
}
