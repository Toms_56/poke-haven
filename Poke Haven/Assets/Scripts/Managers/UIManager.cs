using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    //public Item item;
    //public GameObject ant;

    [SerializeField]
    private Inventory inventoryScript;
    
    //public GameObject panelTeam;
    //public GameObject panelInventory;
    //public GameObject panelMessaging;

    public GameObject pokeballSpawner;
    

    private void Start()
    {
        //inventoryScript = GetComponent<Inventory>();
    }

    public void AddPokemonToInventory(Pokeball pokemonData)
    {
        Debug.Log("La fonction marche " + pokemonData.pokemonData);
        //inventoryScript.pokemonData.Add(pokemonData.pokemonData);
        inventoryScript.AddPokemon(pokemonData.pokemonData);
    }

    public void InstantiatePokeball(GameObject pokeball)
    {
        pokeball = GetComponent<ItemSlot>().pokemonData.prefabPokemon;
        Instantiate(pokeball, pokeballSpawner.transform.position, pokeballSpawner.transform.rotation);
    }

    public void ButtonHit()
    {
        Debug.Log("Button Hit");
    }
    
}
